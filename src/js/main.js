"use strict";

import { createElt, displayPopup, createTable } from './tool.js';

let main = document.getElementById("main");

document.body.id = "add";

let timers = JSON.parse(localStorage.getItem("timer"));


// Lister les dernière séances, avec le nom les répétition et la date
// Update les datas
// Supprimer les données
// Changer la couleur de la page active

// let addBtn = document.getElementById("addBtn");

// addBtn.addEventListener("click", () => {
//     let addSection = document.getElementById("addSection");
//     addSection.classList.remove("hidden");
//     addSection.classList.add("active");
// });

let addBtn = document.getElementById("addBtn");
let listBtn = document.getElementById("listBtn");
let sessionBtn = document.getElementById("sessionBtn");

let addSection = document.getElementById("addSection");
let listSection = document.getElementById("listSection");

addBtn.addEventListener("click", () => {
    addSection.classList.remove("hidden");
    listSection.classList.add("hidden");

    addBtn.classList.add("active");
    listBtn.classList.remove("active");
    sessionBtn.classList.remove("active");
});

listBtn.addEventListener("click", () => {
    addSection.classList.add("hidden");
    listSection.classList.remove("hidden");
    
    listBtn.classList.add("active");
    addBtn.classList.remove("active");
    sessionBtn.classList.remove("active");

    listAllTimer();
});

sessionBtn.addEventListener("click", () => {
    addSection.classList.add("hidden");
    listSection.classList.add("hidden");

    sessionBtn.classList.add("active");
    addBtn.classList.remove("active");
    listBtn.classList.remove("active");
});

let save = document.getElementById("save");

save.addEventListener("click", () => {
    addTimer();
});

function addTimer() {
    let name = document.getElementById("name").value;
    let durationMinute = document.getElementById("durationMinute").value;
    let durationSecond = document.getElementById("durationSecond").value;
    let nb = document.getElementById("nb").value;
    let breakDurationMinute = document.getElementById("breakDurationMinute").value;
    let breakDurationSecond = document.getElementById("breakDurationSecond").value;
    let description = document.getElementById("description").value;

    if (name.length > 0) {
        timers.push({
            "name": name,
            "duration": `${durationMinute}:${durationSecond}`,
            "nb": nb,
            "breakDuration": `${breakDurationMinute}:${breakDurationSecond}`,
            "description": description
        })
        localStorage.setItem("timer", JSON.stringify(timers));
        displayPopup(`Timer ${name} ajouté`);
        listAllTimer();
    } else {
        displayPopup(`Il faut un nom !`);
    }
}

export function act(actionName, rowId) {
    if (actionName === "delete") {
        deleteTimer(rowId);
    }
}

function deleteTimer(rowId) {
    timers.splice(rowId, 1);
    localStorage.setItem("timer", JSON.stringify(timers));
    displayPopup(`Timer supprimé`);
    listAllTimer();
}

function updateTimer(timerName) {}

function listAllTimer() {
    document.body.id = "list";
    listSection.innerHTML = "";
    timers = JSON.parse(localStorage.getItem("timer"));

    listSection.appendChild(createTable(
        ["Nom", "Durée", "Nombre", "Durée de la pause", "description"],
        ["name", "duration", "nb", "breakDuration", "description"],
        timers,
        ["run", "delete", "update"]
    ));
}

function runTimer(name) {
    // Reset
    // Stop | Continue    
}

// Gérer l'absence de tableau